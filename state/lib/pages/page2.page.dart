import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Page2Page extends StatelessWidget {
  const Page2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                child: Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){}),
            MaterialButton(
                child: Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){}),
            MaterialButton(
                child: Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){})
          ],
        ),
      ),
    );
  }
}
