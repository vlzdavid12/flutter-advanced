import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state/providers/user.service.dart';

import '../models/user.dart';

class Page2Page extends StatelessWidget {
  const Page2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: userService.existUser
            ? Text('Name: ${userService.user?.name}')
            : Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                color: Colors.blue[600],
                onPressed: () {
                  final newUser = User(
                      name: 'David',
                      age: 35,
                      profesions: ["FullStack Developer", "Video Game Expert"]
                  );
                  userService.setUser = newUser;
                },
                child: const Text('Establecer Usuario',
                    style: TextStyle(color: Colors.white))),
            MaterialButton(
                color: Colors.blue[600],
                onPressed: () {
                  userService.changeAge(30);
                },
                child: const Text('Cambiar Edad',
                    style: TextStyle(color: Colors.white))),
            MaterialButton(
                color: Colors.blue[600],
                onPressed: () {
                  userService.addProfession();
                },
                child: const Text('Añadir Profesion',
                    style: TextStyle(color: Colors.white)))
          ],
        ),
      ),
    );
  }
}
