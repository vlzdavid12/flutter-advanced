import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state/providers/user.service.dart';

import '../models/user.dart';

class Page1Page extends StatelessWidget {
  const Page1Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userService = Provider.of<UserService>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
        actions: [
          IconButton(
              onPressed: () => userService.removeUser(),
              icon: Icon(Icons.exit_to_app))
        ],
      ),
      body: userService.existUser ? infoUser(user: userService.user) : Center(child: Text('Not found user...'),),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.accessibility_new),
        onPressed: () =>Navigator.pushNamed(context, 'page2'),
      ),
    );
  }
}

class infoUser extends StatelessWidget {
  final user;
  const infoUser({super.key, required this.user});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width:  double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
          Text('General', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ListTile(title: Text('Nombre: ${user.name}')),
          ListTile(title: Text('Edad: ${user.age}')),
          Text('Profesiones', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ...user.profesions
              .map((profesions) => ListTile( title: Text(profesions)))
              .toList(),

        ]
      ),
    );
  }
}
