import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state/pages/page.dart';
import 'package:state/providers/user.service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create:(_) => new UserService())
      ],
      child:  MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'States Flutter',
        initialRoute: 'page1',
        routes: {
          'page1' : (_) => Page1Page(),
          'page2' : (_) => Page2Page(),
        },
      ),
    );
  }
}

