class User{
  String name;
  int? age;
  List<String>? profesions;
  User({required this.name, this.age, this.profesions}): assert(name != null);
}