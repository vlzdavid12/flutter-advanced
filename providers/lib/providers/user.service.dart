import 'package:flutter/material.dart';
import 'package:state/models/user.dart';

class UserService with ChangeNotifier{
  User? _user;
  User? get user => _user;
  bool get existUser => (_user !=  null) ?  true : false;

  set setUser(User user){
    _user = user;
    notifyListeners();
  }

  void changeAge(int age) {
    _user?.age = age;
    notifyListeners();
  }

  void removeUser(){
    _user = null;
    notifyListeners();
  }
  void addProfession(){
    _user?.profesions?.add('Profession ${_user?.profesions!.length?? 0 + 1 }');
    notifyListeners();
  }
}