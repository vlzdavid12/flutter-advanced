import 'package:flutter/material.dart';
import 'package:state/pages/page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'States Flutter',
      initialRoute: 'page1',
      routes: {
        'page1' : (_) => Page1Page(),
        'page2' : (_) => Page2Page(),
      },
    );
  }
}

