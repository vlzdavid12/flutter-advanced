import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:state/providers/user.service.dart';

import '../models/user.dart';

class Page2Page extends StatelessWidget {
  const Page2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: StreamBuilder(
          stream: userService.userStream,
          builder: (BuildContext context, AsyncSnapshot<User> snapshot){
            return Container(
              child: snapshot.hasData ?
              Text('Name: ${snapshot.data?.name}')
              : Text('Pagina 2'),
            );
          },
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                child: Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  final newUser  =  new User(name: 'David', age: 35);
                  userService.loaderUser(newUser);
                }),
            MaterialButton(
                child: Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userService.changeAge(30);
                }),
            MaterialButton(
                child: Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){})
          ],
        ),
      ),
    );
  }
}
