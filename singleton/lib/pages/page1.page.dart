import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:state/models/user.dart';
import 'package:state/providers/user.service.dart';

class Page1Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
      ),
      body: StreamBuilder(
        stream: userService.userStream,
        builder: (BuildContext context, AsyncSnapshot<User> snapshot){
          return snapshot.hasData ? infoUser(snapshot.data!)
              : Center(child: Text('Not Found User...'));
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.accessibility_new),
        onPressed: () => Navigator.pushNamed(context, 'page2'),
      ),
    );
  }
}

class infoUser extends StatelessWidget {
  final User user;

  const infoUser(this.user);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text('General',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Divider(),
        ListTile(title: Text('Nombre: ${user.name}')),
        ListTile(title: Text('Edad: ${user.age}')),
        Text('Profesiones',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Divider(),
        ListTile(title: Text('Profesion 1 ')),
        ListTile(title: Text('Profesion 2 ')),
        ListTile(title: Text('Profesion 3 '))
      ]),
    );
  }
}
