import 'package:get/get.dart';
import 'package:state/models/user.dart';

class UserController extends GetxController{
  var existUser = false.obs;
  var user = User().obs;

  void loaderUser(User pUser){
    this.existUser.value = true;
    this.user.value = pUser;
  }

  void changeAge(int age){
    this.user.update((val) {
      val!.age = age;
    });
  }

  void addProfession(String professions) {
    this.user.update((val) {
      val!.professions = [...val.professions, professions ];
    });
  }
}