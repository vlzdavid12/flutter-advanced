import 'package:flutter/material.dart';
import 'package:state/pages/page.dart';
import 'package:get/get.dart';
void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'States Flutter',
      initialRoute: 'page1',
      getPages: [
        GetPage(name: '/page1', page: () => Page1Page()),
        GetPage(name: '/page2', page: () => Page2Page())
      ],
    );
  }
}

