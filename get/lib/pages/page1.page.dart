import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state/controllers/user.controller.dart';

import '../models/user.dart';

class Page1Page extends StatelessWidget {
  const Page1Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userCTRL = Get.put(UserController());
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
      ),
      body: Obx(() =>  userCTRL.existUser.value ? InfoUser(user: userCTRL.user.value) : NotFound()),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.accessibility_new),
        onPressed: () => Get.toNamed('page2', arguments: {
          'name': 'David',
          'age': 35
        }),
      ),
    );
  }
}

class NotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text('Not Found uSER'),
      ),
    );
  }
}


class InfoUser extends StatelessWidget {
  final User user;

  const InfoUser({super.key, required this.user});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width:  double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
          Text('General', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ListTile(title: Text('Nombre: ${user.name}')),
          ListTile(title: Text('Edad: ${user.age}')),
          Text('Profesiones', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ...user.professions.map((profession) => ListTile(
            title: Text(profession),
          )).toList()


        ]
      ),
    );
  }
}
