import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state/controllers/user.controller.dart';
import 'package:state/models/user.dart';

class Page2Page extends StatelessWidget {
  const Page2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(Get.arguments);
    final userCTRL = Get.find<UserController>();
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                child: Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userCTRL.loaderUser(User(name: 'David', age: 35));
                  Get.snackbar('User success fully', "Insert new name", backgroundColor: Colors.white, boxShadows: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 10
                    )
                  ]);
                }),
            MaterialButton(
                child: Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userCTRL.changeAge(25);
                }),
            MaterialButton(
                child: Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userCTRL.addProfession("New Profession");
                }),
            MaterialButton(
                child: Text('Change Theme', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  Get.changeTheme(Get.isDarkMode ? ThemeData.light() :  ThemeData.dark());
                })
          ],
        ),
      ),
    );
  }
}
