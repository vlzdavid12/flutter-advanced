import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state/bloc/user/user_bloc.dart';

import '../models/user.dart';

class Page2Page extends StatelessWidget {
  const Page2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userBloc = BlocProvider.of<UserBloc>(context, listen: false);
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                child: Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  final newUser = User(
                    name: 'David',
                    age: 36,
                    professions: ["Fullsatck Developer"]
                  );
                 userBloc.add(ActiveUser(newUser));
                }),
            MaterialButton(
                child: Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userBloc.add(ChangeUser(25));
                }),
            MaterialButton(
                child: Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userBloc.add(AddProfession('Nueva Profesion'));
                })
          ],
        ),
      ),
    );
  }
}
