import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state/bloc/user/user_bloc.dart';

import '../models/user.dart';

class Page1Page extends StatelessWidget {
  const Page1Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
        actions: [
          IconButton(
              onPressed: (){
                BlocProvider.of<UserBloc>(context, listen: false).add(DeleteUser());
              },
              icon: const Icon(Icons.delete_outline))
        ],
      ),
      body:
      BlocBuilder<UserBloc, UserState>(
        builder: (context, state){
          return (state.existUser) ? infoUser(user: state.user!) :  const Center(child: Text('Not found user'));
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.accessibility_new),
        onPressed: () =>Navigator.pushNamed(context, 'page2'),
      ),
    );
  }
}

class infoUser extends StatelessWidget {
  final User user;
  const infoUser({super.key, required this.user});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width:  double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
          Text('General', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ListTile(title: Text('Nombre: ${user.name}')),
          ListTile(title: Text('Edad: ${user.age}')),
          Text('Profesiones', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Divider(),
          ...user.professions.map(
                (profession) => ListTile( title: Text(profession))).toList()
        ]
      ),
    );
  }
}
