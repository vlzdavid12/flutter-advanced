import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:state/models/user.dart';

part 'users_state.dart';

class UsersCubit extends Cubit<UsersState> {
  UsersCubit() : super(UsersInitial());

  void selectUser(User user){
    emit(new UserActive(user));
  }

  void changeAge(int age){
    final currenState = state;
    if(currenState is UserActive){
      final newUser = currenState.user.copyWith(age: 30);
      emit(UserActive(newUser));
    }
  }


  void addProfession(){
    final currenState = state;
    if(currenState is UserActive){
      final newProfessions = [
        ...currenState.user.professions!,
        'Profesion ${currenState.user.professions?.length ?? 0 + 1}'
      ];
      final newUser =  currenState.user.copyWith(professions: newProfessions);
      emit(UserActive(newUser));
    }
  }

  void eraseUser(){
    emit(UsersInitial());
  }
}
