part of 'users_cubit.dart';

@immutable
abstract class UsersState {}

class UsersInitial extends UsersState {
  final existUser = false;

  @override
  String toString(){
    return "UserInitial: existUser: false";
  }
}


class UserActive extends UsersState {
  final existUser = true;
  final User user;

  UserActive(this.user);
}