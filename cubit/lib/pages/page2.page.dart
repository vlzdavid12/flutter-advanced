import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state/bloc/user/users_cubit.dart';
import 'package:state/models/user.dart';

class Page2Page extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final userCubit =  context.read<UsersCubit>();
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
                child: Text('Establecer Usuario', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  final newUser =  User(
                    name: 'David Valenzuela',
                    age: 32,
                    professions: [
                      "FullSatack Developer",
                      "VideoGame Full"
                    ]
                  );
                 userCubit.selectUser(newUser);
                }),
            MaterialButton(
                child: Text('Cambiar Edad', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userCubit.changeAge(30);
                }),
            MaterialButton(
                child: Text('Añadir Profesion', style: TextStyle(color: Colors.white)),
                color: Colors.blue[600],
                onPressed: (){
                  userCubit.addProfession();
                })
          ],
        ),
      ),
    );
  }
}
