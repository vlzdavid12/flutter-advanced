import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state/bloc/user/users_cubit.dart';

import 'package:state/models/user.dart';

class Page1Page extends StatelessWidget {
  const Page1Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page 1'),
        actions: [
          IconButton(
              onPressed: () =>context.read<UsersCubit>().eraseUser(),
              icon: Icon(Icons.exit_to_app))
        ],
      ),
      body: BodyScaffold(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.accessibility_new),
        onPressed: () => Navigator.pushNamed(context, 'page2'),
      ),
    );
  }
}

class BodyScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsersCubit, UsersState>(
      builder: (_, state) {
        switch (state.runtimeType) {
          case UsersInitial:
            return const Center(child: Text("Not info of users"));
            break;
          case UserActive:
            return infoUser((state as UserActive).user);
            break;
          default:
            return Center(child: const Text('State not find!!!'));
        }
      },
    );
  }
}

class infoUser extends StatelessWidget {
  final User user;

  const infoUser(this.user);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.all(20.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text('General',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Divider(),
        ListTile(title: Text('Nombre: ${user.name}')),
        ListTile(title: Text('Edad: ${user.age}')),
        Text('Profesiones',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
        Divider(),
        ...user.professions!
            .map((profession) => ListTile(title: Text(profession))).toList(),
      ]),
    );
  }
}
