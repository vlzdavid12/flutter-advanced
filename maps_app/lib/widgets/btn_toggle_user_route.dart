import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/map/map_bloc.dart';

class BtnToggleUserRoute extends StatelessWidget {
  const BtnToggleUserRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final mapBloc = BlocProvider.of<MapBloc>(context);

    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: CircleAvatar(
            maxRadius: 25,
            backgroundColor: Colors.white,
            child: IconButton(
              icon:  Icon(Icons.more_horiz_rounded),
              onPressed: () {
                mapBloc.add(OnToggleUserRoute());
              },
            )
        ));
  }
}
