import 'package:flutter/material.dart';
import 'package:maps_app/delegates/delegates.dart';
class SearchMap extends StatelessWidget {
  const SearchMap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        margin: const EdgeInsets.only(top: 10),
        width:  double.infinity,
        child: GestureDetector(
          onTap: () async {
            final result = await showSearch(context: context, delegate: SearchDestinationDelegate());
            if(result == null) return;

            print(result);
          },
          child:  Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 13),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius:  5,
                  offset: Offset(0,5)
                )
              ]
            ),
            child: const Text('¿Donde Quieres Ir?', style: TextStyle(color: Colors.black54)),
          ),
        ),
      ),
    );
  }
}
