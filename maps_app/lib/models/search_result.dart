class SearchResultModel {
  final bool cancel;
  final bool? manual;

  SearchResultModel({required this.cancel, this.manual});

  // Todo:
  // name, description, LatLng

  @override
  String toString() {
    return '{cancel: $cancel, manual: $manual}';
  }

}