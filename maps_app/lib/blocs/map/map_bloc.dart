import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_app/blocs/blocs.dart';
import 'package:maps_app/themes/themes.dart';

import '../location/location_bloc.dart';

part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {

  final LocationBloc locationBloc;

  GoogleMapController? _mapController;

  late StreamSubscription<LocationState> locationStateSubscription;

  MapBloc({ required this.locationBloc}) :
        super(MapState()) {
    on<OnMapInitializedEvent>(_onInitMap);
    on<OnStartFollowingUserEvent>(_onStartFollowingUser);
    on<OnStopFollowingUserEvent>((event, emit) => emit(state.copyWith(isFollowingUser: false)));
    on<UpdateUserPolyLineEvent>(_onPolyLineNewPoint);
    on<OnToggleUserRoute>((event, emit) => emit(state.copyWith(showMyRoute: !state.showMyRoute)));

     locationStateSubscription = locationBloc.stream.listen((locationState) {
      if(locationState.lastKnowLocation != null){
        add(UpdateUserPolyLineEvent(locationState.myLocationHistory));
      }
      if(!state.isFollowingUser) return;
      if(locationState.lastKnowLocation == null) return;
      moveCamera(locationState.lastKnowLocation!);
    });
  }

  void _onInitMap(OnMapInitializedEvent event, Emitter<MapState> emit) {
      _mapController = event.controller;
      _mapController!.setMapStyle(jsonEncode(uberMapTheme));
      emit(state.copyWith(isMapInitialized: true));
  }


  void moveCamera(LatLng newLocation){
    final cameraUpdate =  CameraUpdate.newLatLng(newLocation);
    _mapController?.animateCamera(cameraUpdate);
  }

  void _onStartFollowingUser(OnStartFollowingUserEvent event, Emitter<MapState> emit){
    if(!state.isFollowingUser) return;
      if(locationBloc.state.lastKnowLocation == null ) return;
      moveCamera(locationBloc.state.lastKnowLocation!);
  }

  void _onPolyLineNewPoint(UpdateUserPolyLineEvent event, Emitter<MapState> emit){
    final myRoute = Polyline(
      polylineId: const PolylineId('myRoute'),
      color: Colors.black45,
      width: 5,
      startCap: Cap.roundCap,
      endCap:  Cap.roundCap,
      points: event.userLocations
    );

    final currentPolyLines = Map<String, Polyline>.from(state.polyLines);
    currentPolyLines['myRoute'] =  myRoute;
    emit(state.copyWith(polyLines: currentPolyLines));
  }

  @override
  Future<void> close() {
    locationStateSubscription.cancel();
    return super.close();
  }


}
