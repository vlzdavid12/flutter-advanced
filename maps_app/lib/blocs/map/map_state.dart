part of 'map_bloc.dart';

class MapState extends Equatable {
  final bool isMapInitialized;
  final bool isFollowingUser;
  final bool showMyRoute;

  final Map<String, Polyline> polyLines;
  final Map<String, Marker> markers;

  MapState(
      {this.isMapInitialized = false,
      this.isFollowingUser = true,
      this.showMyRoute = true,
      Map<String, Polyline>? polyLines,
      Map<String, Marker>? markers})
      : polyLines = polyLines ?? const {},
        markers = markers ?? const {};

  MapState copyWith(
          {bool? isMapInitialized,
          bool? isFollowingUser,
          bool? showMyRoute,
          Map<String, Polyline>? polyLines,
          Map<String, Marker>? markers}) =>
      MapState(
          isMapInitialized: isMapInitialized ?? this.isMapInitialized,
          isFollowingUser: isFollowingUser ?? this.isFollowingUser,
          showMyRoute:  showMyRoute ?? this.showMyRoute,
          polyLines: polyLines ?? this.polyLines,
          markers: markers ?? this.markers);

  @override
  List<Object> get props => [isMapInitialized, isFollowingUser, showMyRoute, polyLines];
}
