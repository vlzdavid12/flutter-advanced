import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:maps_app/blocs/blocs.dart';
import 'package:meta/meta.dart';
import 'package:permission_handler/permission_handler.dart';

part 'gps_event.dart';
part 'gps_state.dart';

class GpsBloc extends Bloc<GpsEvent, GpsState> {

  StreamSubscription? gpsServiceSubcription;

  GpsBloc() : super(GpsState(isGpsEnable: false, isGpsPermission:  false)) {
    on<GpsAndPermissionEvent>((event, emit) => emit(state.copyWith(
      isGpsEnable: event.isGpsEnable,
      isGpsPermission: event.isGpsPermission
    )));
    _init();
  }

  Future<void> _init() async{

    final gpsInitStatus = await Future.wait([
      _checkGpsStatus(),
      _isPermissionsGranted()
    ]);

    add(GpsAndPermissionEvent(isGpsEnable: gpsInitStatus[0], isGpsPermission: gpsInitStatus[1]));
  }

  Future<bool> _isPermissionsGranted() async {
    final isGranted =  await Permission.location.isGranted;
    return isGranted;
  }

  Future<bool> _checkGpsStatus() async{
    final isEnable = await  Geolocator.isLocationServiceEnabled();
    gpsServiceSubcription = Geolocator.getServiceStatusStream().listen((event) {
      final isEnabled =  (event.index == 1) ? true : false;
      print('Service status $isEnabled');
      add(GpsAndPermissionEvent(isGpsEnable: isEnabled, isGpsPermission: state.isGpsPermission));
    });
    return isEnable;
  }

  Future<void> askGpsAccess() async {
    final status = await Permission.location.request();
    switch(status){
      case PermissionStatus.granted:
        add(GpsAndPermissionEvent(isGpsEnable: state.isGpsEnable, isGpsPermission: true));
        break;
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.limited:
      case PermissionStatus.permanentlyDenied:
        add(GpsAndPermissionEvent(isGpsEnable: state.isGpsEnable, isGpsPermission: false));
        openAppSettings();
        break;
    }
  }

  @override
  Future<void> close() {
    gpsServiceSubcription?.cancel();
    return super.close();
  }
}


