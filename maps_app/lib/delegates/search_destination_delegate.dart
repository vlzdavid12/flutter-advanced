import 'package:flutter/material.dart';
import 'package:maps_app/models/models.dart';

class SearchDestinationDelegate extends SearchDelegate<SearchResultModel> {

  SearchDestinationDelegate():super(
      searchFieldLabel: 'Buscar...'
  );


  @override
  List<Widget>? buildActions(BuildContext context) {
      return  [
        IconButton(
            icon: const Icon(Icons.clear),
            onPressed: (){
              query = '';
            }),
       ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back_ios),
      onPressed: (){
        final result = SearchResultModel(cancel: true);
        close(context, result);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
      return const Text('BuildResults');
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          leading: const Icon(Icons.location_on_outlined, color: Colors.black45),
          title: const Text('Colocar la ubicación manualmente', style: TextStyle(color: Colors.black54)),
          onTap: (){
            final result = SearchResultModel(cancel: false, manual:  true);
            close(context, result);
          },
        )
      ],
    );
  }

}
