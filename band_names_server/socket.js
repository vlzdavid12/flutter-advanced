const {io}  = require('./index.js');
const Band = require('./models/band.js');
const Bands = require('./models/bands.js');
const bands =  new Bands();

bands.addBand(new Band('Queen'));
bands.addBand(new Band('Bon Jovi'));
bands.addBand(new Band('Metallica'));
bands.addBand(new Band('Heroes del Silecio'));

io.on('connection', client => {
    console.log("Cliente conectado");
    client.emit('active-bands', bands.getBands());

    client.on('diconnect', () => console.log("Cliente desconectado"));
    client.on('message', (payload) => {
        //All Connect
        io.emit('allMessage', {admin: 'New Mensage'});
    });
    client.on('emit-message', (payload)=>{
            //console.log(payload);
            //io.emit('new-message', payload)//Emit ALL
            client.broadcast.emit('new-message', payload) // Emit All never emit
    });
    client.on('vote-band', (payload) => {
        bands.voteBand(payload.id);
        io.emit('active-bands', bands.getBands());
    })
    client.on('add-band', (payload) => {
        const newBand = new Band(payload.name);
        bands.addBand(newBand);
        io.emit('active-bands', bands.getBands());
    })
    client.on('delete-band', (payload) => {
        bands.deleteBands(payload.id);
        io.emit('active-bands', bands.getBands());
    })
});