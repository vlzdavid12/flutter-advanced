const Band = require("./band");

class Bands {
    constructor(){
        this.bands = [];
    }

    addBand(band = new Band()){
        this.bands.push(band);
    }

    getBands(){
        return this.bands;
    }

    deleteBands(id = ''){
        this.bands =  this.bands.filter(b => b.id != id)
        return this.bands;
    }

    voteBand(id = ''){
        this.bands.map(b => {
            if(b['id'] == id){
                b['vote']++
                return;
            }else{
                return this.bands;
            }
     
         
        })
    }
}

module.exports = Bands;