import 'dart:io';

import 'package:bandnames/providers/socket_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';
import '../models/band.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Band> bands = [];
/*
  List<Band> bands = [
    Band(id: '1', name: 'Metallica', vote: 5),
    Band(id: '2', name: 'Queen', vote: 1),
    Band(id: '3', name: 'Heroes del silencio', vote: 2),
    Band(id: '4', name: 'Bon Jovi', vote: 5),
  ];
*/

  @override
  void initState(){
    final socketService =  Provider.of<SocketService>(context, listen: false);
    socketService.socket?.on('active-bands', _handleActiveBands);
    super.initState();
  }

  _handleActiveBands(dynamic payload){
    bands = (payload as List)
        .map((band) => Band.fromMap(band))
        .toList();

    setState(() {});
  }

  @override
  void dispose(){
    final socketService =  Provider.of<SocketService>(context, listen: false);
    socketService.socket?.off('active-bands');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final socketService =  Provider.of<SocketService>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        title: Text('BandNames', style: TextStyle(color: Colors.black87)),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Container(
            margin:  EdgeInsets.only(right: 10),
            child:
            (socketService.serverStatus == ServerStatus.Online)?
            Icon(Icons.check_circle, color: Colors.blue[300])
            : Icon(Icons.offline_bolt, color: Colors.red),
          )
        ],
      ),
      body: Column(
        children: [
          _showGraph(),
          Expanded(child: ListView.builder(
             itemCount: bands.length,
             itemBuilder: (BuildContext context, int index) =>
                 _buildListTile(bands[index])))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        elevation: 1,
        onPressed: addNewBank,
      ),
    );
  }

  Widget _buildListTile(Band band) {
    final socketService =  Provider.of<SocketService>(context, listen: false);
    return
      Dismissible(
          key: Key(band.id!),
          direction: DismissDirection.startToEnd,
          onDismissed: (_) => socketService.emit('delete-band',{'id': band.id}),
          background: Container(
            padding: EdgeInsets.only(left: 8.0),
            color: Colors.red,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text('Delete Band', style: TextStyle(color: Colors.white)),
            )
          ),
          child: ListTile(
            leading: CircleAvatar(
              child: Text(band.name!.substring(0, 2)),
              backgroundColor: Colors.blue[100],
            ),
            title: Text(band.name!),
            trailing: Text('${band.vote}', style: TextStyle(fontSize: 20)),
            onTap:  () =>socketService.socket?.emit('vote-band', {'id': band.id}),
          ));
  }

  addNewBank(){
    final textController =  new TextEditingController();

    if(Platform.isAndroid) {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
              title: Text('New band name'),
              content: TextField(controller: textController,),
              actions: <Widget>[
                MaterialButton(
                    child: Text('Add'),
                    elevation: 5,
                    textColor: Colors.blue,
                    onPressed: () => addBandToList(textController.text))
              ],
            ));
    }else{
      showCupertinoDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text('New band name'),
                content: CupertinoTextField(
                    controller: textController
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    isDefaultAction: true,
                    child: Text('Add'),
                    onPressed: () => addBandToList(textController.text),
                  ),
                  CupertinoDialogAction(
                    isDestructiveAction: true,
                    child: Text('Dismiss'),
                    onPressed: () => Navigator.pop(context),
                  )
                ]
            ));
    }

  }
  
  void addBandToList(String name){
    if(name.length > 1){
      final socketService =  Provider.of<SocketService>(context, listen: false);
      socketService.emit('add-band', { 'name': name });
    }
    Navigator.pop(context);
  }

  Widget _showGraph(){
    Map<String, double> dataMap = {};
    bands.map((band) => {
      dataMap.putIfAbsent(band.name.toString(), () => (band.vote?? 0).toDouble())
    }).toList();

    final List<Color> colorList = [
      Colors.blue[50]!,
      Colors.blue[200]!,
      Colors.pink[50]!,
      Colors.pink[200]!,
      Colors.yellow[50]!,
      Colors.pink[200]!

    ];
    return Container(
      width: double.infinity,
      height: 280,
      child: PieChart(dataMap: dataMap,
        animationDuration: Duration(milliseconds: 800),
        chartLegendSpacing: 16,
        chartRadius: MediaQuery.of(context).size.width / 2.0,
        colorList: colorList,
        initialAngleInDegree: 0,
        ringStrokeWidth: 32,
        legendOptions: LegendOptions(
          showLegendsInRow: false,
          legendPosition: LegendPosition.right,
          showLegends: true,
          legendTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        chartValuesOptions: ChartValuesOptions(
          showChartValueBackground: false,
          showChartValues: true,
          showChartValuesInPercentage: true,
          showChartValuesOutside: false,
          decimalPlaces: 1,
        ),),

    );
  }
}
