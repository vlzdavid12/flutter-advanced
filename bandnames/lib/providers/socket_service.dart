import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
enum ServerStatus{
  Online,
  Offline,
  Connecting
}
class SocketService with ChangeNotifier {
  ServerStatus _serverStatus =  ServerStatus.Connecting;
  IO.Socket? _socket;

  ServerStatus get serverStatus => this._serverStatus;

  IO.Socket? get socket => _socket;
  get emit => _socket?.emit;

  SocketService() {
    _initConfig();
  }

  void _initConfig(){
     _socket = IO.io('http://192.168.20.28:3000',{
      'transports': ['websocket'],
      'autoConnect': true
    });
    _socket?.onConnect((_)  {
      _serverStatus = ServerStatus.Online;
      print('connect');
      notifyListeners();
    });
    _socket?.onDisconnect((_) {
      _serverStatus = ServerStatus.Offline;
      print('disconnect');
      notifyListeners();
    });
  /*  _socket.on('new-message', (payload) {
      print('New Message: $payload');
      print('Name:' + payload['name']);
      print('Message: ' + payload['message']);
      print(payload.containsKey('message2')?payload['message2'] : null);
    });*/
  }
}